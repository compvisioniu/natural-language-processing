# README #

This repository contains some information to start NLP with, and drafts of some applications which may be useful as examples, or projects to continue developing.

### Who do I talk to? ###

* Ugulava George BS-4 - main contributor.
* Stanislav Protasov - team lead.
* Adil Khan - head of deep learning lab.