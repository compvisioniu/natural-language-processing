
# coding: utf-8

# In[1]:

import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
from collections import Counter
import matplotlib.pyplot as plt
get_ipython().magic('pylab inline')
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from tensorflow.python.framework import dtypes
from tensorflow.contrib import learn as tflearn
from tensorflow.contrib import layers as tflayers
from lstm_model import lstm_model, convert_data

data = pd.read_csv('text_emotion.csv')
names = data.columns.values.tolist()


# In[2]:

#Preprocessing, select 5 most frequen labels and map them to float number
#fro purpose of easy labeling for nn.
baseline = ['sadness', 'worry', 'happiness', 'love', 'neutral']
data = data[[(sentiment in baseline) for sentiment in data['sentiment']]]
data['sentiment'] = data['sentiment'].map({'sadness' : 1.0, 'worry' : 2.0,
                        'happiness' : 3.0, 'love' : 4.0, 'neutral' : 5.0})
train_data, test_data = train_test_split(data, test_size=0.2)


# In[3]:

def draw_histogram(data):
    labels,values = zip(*Counter(data['sentiment']).items())
    indexes = np.arange(len(labels))
    width = 0.5
    plt.xticks(indexes + width * 0.5, labels, rotation = 'vertical')
    plt.bar(indexes, values, width)


# In[4]:

#distribution of train data
draw_histogram(train_data)


# In[5]:

#distribution of test_data
draw_histogram(test_data)


# In[6]:

#converting data to fit the shape appropriate for rnn which expects:
#tensor[B,T....] where B - batch size, T - Timesteps, and then goes features
#X_train_TF, X_test_TF - only tf-idf set features for classification
#TODO inrich set of features lately
vectorizer = TfidfVectorizer(stop_words = 'english')
X_train_TF = convert_data(pd.DataFrame(vectorizer.fit_transform(train_data['content']).todense()), 3)
X_test_TF = convert_data(pd.DataFrame(vectorizer.transform(test_data['content']).todense()), 3)
Y_train = train_data['sentiment']
Y_test = test_data['sentiment']
#batched_train_X_TF = tf.train.batch(tensors = X_train_TF.tolist(), 
#                                    batch_size=100,name="train_batch")
#batched_test_X_TF = tf.train.batch(tensors = X_test_TF.tolist(), 
#                                   batch_size=100,name="trest_batch")


# In[7]:

print(X_train_TF.shape,'\n')
print(X_test_TF.shape, '\n')


# In[8]:

timesteps = 3
rnn_layers = [{'num_units':5}]
dense_layers= None
training_steps = 33978
batch_size = 100

classificator = tf.contrib.learn.Estimator(model_fn=lstm_model(timesteps,rnn_layers,dense_layers))


# In[9]:

validation_monitor = tf.contrib.learn.monitors.ValidationMonitor(X_train_TF, Y_train, 
                                                    every_n_steps=1000, early_stopping_rounds = 1000)


# In[10]:

classificator.fit(X_train_TF, Y_train, monitors = [validation_monitor],
                  batch_size = batch_size, steps = training_steps)


# In[ ]:



