import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.python.framework import dtypes
from tensorflow.contrib import learn as tflearn
from tensorflow.contrib import layers as tflayers

'''Created by George 11/1/2016'''


def convert_data(data, time_steps, labels = False):
    data_frame = []
    for i in range(len(data) - time_steps):
        if labels:
            try:
                data_frame.append(data.iloc[i+time_steps].as_matrix())
            except AttributeError:
                data_frame.append(data.iloc[i+time_steps])
        else:
            data_ = data.iloc[i:i+time_steps].as_matrix()
            data_frame.append(data_ if len(data_.shape)>1 else [[i] for i in data_])
    return np.array(data_frame, dtype=np.float32)



"""
LSTM model creation based on: stacked lstm cells and optional dense layers
@param num_units is the size of the cells
@param rnn_layer is the list of int or dict :
                            1) list of init the steps are used to instantiate LSTMCells
                            2)list of dict:[{steps : int, kee_prob: int}, ...]
@param dense_layers is list of nodes for each layer
@param return  is model itself.
"""


def lstm_model(num_utnits, rnn_layers, dense_layers=None, learning_rate=0.1, optimizer='Adagrad'):
    def lstm_cells(layers):
        if isinstance(layers[0], dict):
            return [tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.LSTMCell(layer['num_units'],
                                                                          state_is_tuple=True),
                                                  layer['keep_prob'])
                    if layer.get('keep_prob') else tf.nn.rnn_cell.LSTMCell(layer['num_units'],
                                                                           state_is_tuple=True)
                    for layer in layers]
        return [tf.nn.rnn_cell.LSTMCell(steps, state_is_tuple=True) for steps in layers]

    def lstm_layers(input_layers, layers):
        if layers and isinstance(layers, dict):
            return tflayers.stack(input_layers, tflayers.fully_connected,
                                  layers['layers'])  # check later
        elif layers:
            return tflayers.stack(input_layers, tflayers.fully_connected, layers)
        else:
            return input_layers

    def model(X, y):
        stacked_lstm = tf.nn.rnn_cell.MultiRNNCell(lstm_cells(rnn_layers), state_is_tuple=True)
        #unpacked = tf.unpack(X,axis=1, num=num_utnits)
        output, layers = tf.nn.dynamic_rnn(cell=stacked_lstm, inputs=X, dtype=dtypes.float32,)
        output = lstm_layers(output[-1], dense_layers)
        prediction, loss = tflearn.run_n({"outputs": output, "last_states": layers}, n=1,
                                        feed_dict=None)
        train_operation = tflayers.optimize_loss(loss, tf.contrib.framework.get_global_step(), optimizer=optimizer,
                                                 learning_rate=learning_rate)
        return prediction, loss, train_operation

    return model
